#ifndef QUEMU_VIO_H
#define QUEMU_VIO_H

#include <linux/types.h>

//#define DEBUG_QEMUVIO

#ifdef DEBUG_QEMUVIO
void printSwapBuff(struct swap_buff *buf);
void printConfigs(void *devdata);

#define pdebug(fmt, ...) printf(fmt,##__VA_ARGS__)
#else
#define pdebug(fmt, ...) ({ if (0) printf(fmt, ##__VA_ARGS__); 0; })
#endif

#define toSeconds(t) (t.tv_sec + (t.tv_usec/1000000.))


#endif /* QUEMU_VIO_H */
