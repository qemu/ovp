#include <stdio.h>
#include <stdint.h>


#include <qemu-common.h>
#include <sysemu.h>
#include "qemu-vio-blk.h"
#include "block_int.h"
#ifdef __linux__
# include <scsi/sg.h>
#endif

extern char *progname;
extern unsigned long int num_reads;
extern unsigned long int num_writes;
extern unsigned long int num_scsi_cmds;
extern unsigned long int num_flushs;
extern unsigned long int bytes_wrote;
extern unsigned long int bytes_read;

char multiwrite_async_ret = 0;


//#define trace() printf("qemu-vio-blk: %s-%s: %s(%d)\n",__TIME__, __FILE__,__func__, __LINE__)
#define trace() ;

static void qemu_vio_blk_req_complete(VirtIOBlockReq *req, int status)
{

    if (req->out->type & VIRTIO_BLK_T_FLUSH) {
        bytes_wrote+=req->qiov.size;
    } else if (req->out->type & VIRTIO_BLK_T_SCSI_CMD) {
        bytes_wrote+=req->qiov.size;
    } else if (req->out->type & VIRTIO_BLK_T_OUT) {
        bytes_wrote+=req->qiov.size;
    } else {
        bytes_read+=req->qiov.size;
    }

    VirtIOBlock *s = req->dev;
    trace();
    req->in->status = status;
    virtqueue_push(s->vq, &req->elem, req->qiov.size + sizeof(*req->in));

    if (req)
        free(req);
}

static void qemu_vio_blk_flush_complete(void *opaque, int ret)
{
    VirtIOBlockReq *req = opaque;
    trace();
    qemu_vio_blk_req_complete(req, ret ? VIRTIO_BLK_S_IOERR : VIRTIO_BLK_S_OK);
}

#ifdef __linux__
static void qemu_vio_blk_handle_scsi(VirtIOBlockReq *req)
{
    struct sg_io_hdr hdr;
    int ret, size = 0;
    int status;
    int i;
    trace();
    /*
     * We require at least one output segment each for the virtio_blk_outhdr
     * and the SCSI command block.
     *
     * We also at least require the virtio_blk_inhdr, the virtio_scsi_inhdr
     * and the sense buffer pointer in the input segments.
     */
    if (req->elem.out_num < 2 || req->elem.in_num < 3) {
        qemu_vio_blk_req_complete(req, VIRTIO_BLK_S_IOERR);
        return;
    }

    /*
     * No support for bidirection commands yet.
     */
    if (req->elem.out_num > 2 && req->elem.in_num > 3) {
        qemu_vio_blk_req_complete(req, VIRTIO_BLK_S_UNSUPP);
        return;
    }

    /*
     * The scsi inhdr is placed in the second-to-last input segment, just
     * before the regular inhdr.
     */
    req->scsi = (void *)req->elem.in_sg[req->elem.in_num - 2].iov_base;
    size = sizeof(*req->in) + sizeof(*req->scsi);

    memset(&hdr, 0, sizeof(struct sg_io_hdr));
    hdr.interface_id = 'S';
    hdr.cmd_len = req->elem.out_sg[1].iov_len;
    hdr.cmdp = req->elem.out_sg[1].iov_base;
    hdr.dxfer_len = 0;

    if (req->elem.out_num > 2) {
        /*
         * If there are more than the minimally required 2 output segments
         * there is write payload starting from the third iovec.
         */
        hdr.dxfer_direction = SG_DXFER_TO_DEV;
        hdr.iovec_count = req->elem.out_num - 2;

        for (i = 0; i < hdr.iovec_count; i++)
            hdr.dxfer_len += req->elem.out_sg[i + 2].iov_len;

        hdr.dxferp = req->elem.out_sg + 2;

    } else if (req->elem.in_num > 3) {
        /*
         * If we have more than 3 input segments the guest wants to actually
         * read data.
         */
        hdr.dxfer_direction = SG_DXFER_FROM_DEV;
        hdr.iovec_count = req->elem.in_num - 3;
        for (i = 0; i < hdr.iovec_count; i++)
            hdr.dxfer_len += req->elem.in_sg[i].iov_len;

        hdr.dxferp = req->elem.in_sg;
        size += hdr.dxfer_len;
    } else {
        /*
         * Some SCSI commands don't actually transfer any data.
         */
        hdr.dxfer_direction = SG_DXFER_NONE;
    }

    hdr.sbp = req->elem.in_sg[req->elem.in_num - 3].iov_base;
    hdr.mx_sb_len = req->elem.in_sg[req->elem.in_num - 3].iov_len;
    size += hdr.mx_sb_len;

    ret = bdrv_ioctl(req->dev->bs, SG_IO, &hdr);
    if (ret) {
        status = VIRTIO_BLK_S_UNSUPP;
        hdr.status = ret;
        hdr.resid = hdr.dxfer_len;
    } else if (hdr.status) {
        status = VIRTIO_BLK_S_IOERR;
    } else {
        status = VIRTIO_BLK_S_OK;
    }

    req->scsi->errors = hdr.status;
    req->scsi->residual = hdr.resid;
    req->scsi->sense_len = hdr.sb_len_wr;
    req->scsi->data_len = hdr.dxfer_len;

    qemu_vio_blk_req_complete(req, status);
}
#else
static void qemu_vio_blk_handle_scsi(VirtIOBlockReq *req)
{
    qemu_vio_blk_req_complete(req, VIRTIO_BLK_S_UNSUPP);
}
#endif /* __linux__ */

BlockErrorAction drive_get_on_error(
    BlockDriverState *bdrv, int is_read)
{
    trace();
    /*
    DriveInfo *dinfo;

    QTAILQ_FOREACH(dinfo, &drives, next) {
        if (dinfo->bdrv == bdrv)
            return is_read ? dinfo->on_read_error : dinfo->on_write_error;
    }

    return is_read ? BLOCK_ERR_REPORT : BLOCK_ERR_STOP_ENOSPC;
    */
    return BLOCK_ERR_REPORT;
}

static void q_vm_stop(int reason)
{
    trace();

}

static int qemu_vio_blk_handle_rw_error(VirtIOBlockReq *req, int error,
                                        int is_read)
{
    trace();
    BlockErrorAction action =
        drive_get_on_error(req->dev->bs, is_read);
    VirtIOBlock *s = req->dev;


    if (action == BLOCK_ERR_IGNORE)
        return 0;

    if ((error == ENOSPC && action == BLOCK_ERR_STOP_ENOSPC)
            || action == BLOCK_ERR_STOP_ANY) {
        req->next = s->rq;
        s->rq = req;
        q_vm_stop(0);
    } else {

        qemu_vio_blk_req_complete(req, VIRTIO_BLK_S_IOERR);

    }


    return 1;
}

static void qemu_vio_blk_rw_complete(void *opaque, int ret)
{
    VirtIOBlockReq *req = opaque;
    int is_read = !(req->out->type & VIRTIO_BLK_T_OUT);

    trace();

    if (!is_read)
        multiwrite_async_ret++;

    if (ret) {
        if (qemu_vio_blk_handle_rw_error(req, -ret, is_read))
            return;
    }

    qemu_vio_blk_req_complete(req, VIRTIO_BLK_S_OK);
}

void do_aio_multiwrite(BlockDriverState *bs, MultiReqBuffer *mrb)
{
    int i, ret;

    trace();
    if (!mrb->num_writes) {
         return;
    }

    trace();
    ret = bdrv_aio_multiwrite(bs, mrb->blkreq, mrb->num_writes);
    trace();

    if (ret != 0) {
        for (i = 0; i < mrb->num_writes; i++) {
             if (mrb->blkreq[i].error) {
                 qemu_vio_blk_rw_complete(mrb->blkreq[i].opaque, -EIO);
             }
         }
    }
    trace();

    while (multiwrite_async_ret < mrb->num_writes)
        qemu_aio_wait();

    trace();
    multiwrite_async_ret = 0;
    mrb->num_writes = 0;
}


void do_multiwrite(BlockDriverState *bs, BlockRequest *blkreq,
                   int num_writes)
{
    int i, ret;
    trace();

    for (i = 0; i < num_writes; i++) {

        ret = bdrv_write(bs,blkreq[i].sector,blkreq[i].qiov->iov->iov_base,blkreq[i].nb_sectors);
        blkreq[i].cb(blkreq[i].opaque,ret);
    }

    return;
}

static void qemu_vio_blk_handle_write(VirtIOBlockReq *req, MultiReqBuffer *mrb)
{
    BlockRequest *blkreq;

/*    if (req->out->sector & req->dev->sector_mask) {
        virtio_blk_rw_complete(req, -EIO);
        return;
    }
*/
    if (mrb->num_writes == 32) {
        async_context_push();
        do_aio_multiwrite(req->dev->bs, mrb);
        async_context_pop();
        mrb->old_bs = req->dev->bs;
    }

    blkreq = &mrb->blkreq[mrb->num_writes];
    blkreq->sector = req->out->sector;
    blkreq->nb_sectors = req->qiov.size / 512;
    blkreq->qiov = &req->qiov;
    blkreq->cb = qemu_vio_blk_rw_complete;
    blkreq->opaque = req;
    blkreq->error = 0;

    mrb->num_writes++;
}

static void qemu_vio_blk_handle_read(VirtIOBlockReq *req)
{
    BlockDriverAIOCB *acb;

    trace();
    req->in->status = WAITING_ASYNC_RET;

    acb = bdrv_aio_readv(req->dev->bs, req->out->sector, &req->qiov,
                         req->qiov.size / 512, qemu_vio_blk_rw_complete, req);

    trace();
    if (!acb) {
        qemu_vio_blk_rw_complete(req, -EIO);
    }

    trace();
    while (req->in->status == WAITING_ASYNC_RET)
        qemu_aio_wait();

    trace();
    return;
}

static void qemu_vio_blk_handle_flush(BlockRequest *blkreq, int *num_writes,
                                      VirtIOBlockReq *req, BlockDriverState **old_bs)
{
    BlockDriverAIOCB *acb;
    trace();
    /*
     * Make sure all outstanding writes are posted to the backing device.
     */
    if (*old_bs != NULL) {

        do_multiwrite(*old_bs, blkreq, *num_writes);
    }
    *num_writes = 0;
    *old_bs = req->dev->bs;

    acb = bdrv_aio_flush(req->dev->bs, qemu_vio_blk_flush_complete, req);
    if (!acb) {
        qemu_vio_blk_req_complete(req, VIRTIO_BLK_S_IOERR);
    }
}

VirtIOBlockReq *virtio_blk_get_request(VirtIOBlock *s)
{

    VirtIOBlockReq *req = malloc(sizeof(VirtIOBlockReq));

    trace();

    bzero(req,sizeof(VirtIOBlockReq));

    req->dev = s;
    req->dev->bs = s->bs;

    if (req != NULL) {
        if (!virtqueue_pop(s->vq, &req->elem)) {
            free(req);
            return NULL;
        }
    }

    return req;
}


void virtio_blk_handle_request(VirtIOBlockReq *req,
                               MultiReqBuffer *mrb)
{

    trace();
    if (req->elem.out_num < 1 || req->elem.in_num < 1) {
        fprintf(stderr, "qemu-vio-blk: missing headers\n");
        exit(1);
    }

    if (req->elem.out_sg[0].iov_len < sizeof(*req->out) ||
            req->elem.in_sg[req->elem.in_num - 1].iov_len < sizeof(*req->in)) {
        fprintf(stderr, "qemu-vio-blk: header not in correct element\n");
        exit(1);
    }

    req->out = (void *)req->elem.out_sg[0].iov_base;
    req->in = (void *)req->elem.in_sg[req->elem.in_num - 1].iov_base;

    if (req->out->type & VIRTIO_BLK_T_FLUSH) {
        num_flushs++;
        qemu_vio_blk_handle_flush(mrb->blkreq, &mrb->num_writes,
                                  req, &mrb->old_bs);
    } else if (req->out->type & VIRTIO_BLK_T_SCSI_CMD) {
        num_scsi_cmds++;
        qemu_vio_blk_handle_scsi(req);
    } else if (req->out->type & VIRTIO_BLK_T_OUT) {
        num_writes++;
        qemu_iovec_init_external(&req->qiov, &req->elem.out_sg[1],
                                 req->elem.out_num - 1);
        qemu_vio_blk_handle_write(req, mrb);
    } else {
        num_reads++;
        qemu_iovec_init_external(&req->qiov, &req->elem.in_sg[0],
                                 req->elem.in_num - 1);
        async_context_push();
        qemu_vio_blk_handle_read(req);
        async_context_pop();
    }
}

void qemu_vio_guess_geometry(BlockDriverState *bs, int *pcyls, int *pheads, int *psecs)
{
    bdrv_guess_geometry(bs, pcyls, pheads, psecs);
}

int64_t qemu_vio_getlength(BlockDriverState *bs)
{
    return bdrv_getlength(bs) / 512;
}
