#ifndef QUEMU_VIO_VIRTIO_H
#define QUEMU_VIO_VIRTIO_H

#include <stdint.h>

#include "targphys.h"
#include "block.h"
#include "sysemu.h"
#include "targphys.h"

#include <linux/virtio_blk.h>

#define VIRTQUEUE_MAX_SIZE 1024
#define BLOCK_SERIAL_STRLEN 20

#define VIRTIO_BLK_S_OK         0
#define VIRTIO_BLK_S_IOERR      1
#define VIRTIO_BLK_S_UNSUPP     2

#define VIRTQUEUE_MAX_SIZE 1024
#define VIRTIO_PCI_VRING_ALIGN         4096


#define VIRTIO_CONFIG_S_ACKNOWLEDGE     1
/* We have found a driver for the device. */
#define VIRTIO_CONFIG_S_DRIVER          2
/* Driver has used its parts of the config, and is happy */
#define VIRTIO_CONFIG_S_DRIVER_OK       4
/* We've given up on this device. */
#define VIRTIO_CONFIG_S_FAILED          0x80

/* Some virtio feature bits (currently bits 28 through 31) are reserved for the
		 * transport being used (eg. virtio_ring), the rest are per-device feature bits. */
#define VIRTIO_TRANSPORT_F_START        28
#define VIRTIO_TRANSPORT_F_END          32

/* We notify when the ring is completely used, even if the guest is suppressing
 * callbacks */
#define VIRTIO_F_NOTIFY_ON_EMPTY        24
/* We support indirect buffer descriptors */
#define VIRTIO_RING_F_INDIRECT_DESC     28
/* A guest should never accept this.  It implies negotiation is broken. */
#define VIRTIO_F_BAD_FEATURE		30

/* from Linux's linux/virtio_ring.h */

/* This marks a buffer as continuing via the next field. */
#define VRING_DESC_F_NEXT       1
/* This marks a buffer as write-only (otherwise read-only). */
#define VRING_DESC_F_WRITE      2
/* This means the buffer contains a list of buffer descriptors. */
#define VRING_DESC_F_INDIRECT  4

/* This means don't notify other side when buffer added. */
#define VRING_USED_F_NO_NOTIFY  1
/* This means don't interrupt guest when buffer consumed. */
#define VRING_AVAIL_F_NO_INTERRUPT      1

#ifndef offsetof
#define offsetof(TYPE, MEMBER) ((size_t) &((TYPE *) 0)->MEMBER)
#endif
#ifndef container_of
#define container_of(ptr, type, member) ({                      \
        const typeof(((type *) 0)->member) *__mptr = (ptr);     \
        (type *) ((char *) __mptr - offsetof(type, member));})
#endif

#ifndef MIN
#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#endif
#ifndef MAX
#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#endif

/* This is the last element of the write scatter-gather list */
struct virtio_blk_inhdr
{
    unsigned char status;
};

/* SCSI pass-through header */
/*
struct virtio_scsi_inhdr
{
    uint32_t errors;
    uint32_t data_len;
    uint32_t sense_len;
    uint32_t residual;
};
*/

typedef struct {
    void (*notify)(void * opaque, uint16_t vector);
    void (*save_config)(void * opaque, QEMUFile *f);
    void (*save_queue)(void * opaque, int n, QEMUFile *f);
    int (*load_config)(void * opaque, QEMUFile *f);
    int (*load_queue)(void * opaque, int n, QEMUFile *f);
    unsigned (*get_features)(void * opaque);
} VirtIOBindings;

struct myiovec {
    void *iov_base;
    size_t iov_len;
};

typedef struct VRingDesc
{
    uint64_t addr;
    uint32_t len;
    uint16_t flags;
    uint16_t next;
	uint32_t poolidx;
	uint32_t offset;
} VRingDesc;

typedef struct VRingAvail
{
    uint16_t flags;
    uint16_t idx;
    uint16_t ring[0];
} VRingAvail;

typedef struct VRingUsedElem
{
    uint32_t id;
    uint32_t len;
} VRingUsedElem;

typedef struct VRingUsed
{
    uint16_t flags;
    uint16_t idx;
    VRingUsedElem ring[0];
} VRingUsed;

typedef struct VRing
{
    unsigned int num;
    target_phys_addr_t desc;
    target_phys_addr_t avail;
    target_phys_addr_t used;
} VRing;

typedef struct VirtQueue
{
    VRing vring;
    target_phys_addr_t pa;
    uint16_t last_avail_idx;
    int inuse;
    uint16_t vector;
//    void (*handle_output)(VirtIODevice *vdev, VirtQueue *vq);
} VirtQueue;

typedef struct VirtQueueElement
{
    unsigned int index;
    unsigned int out_num;
    unsigned int in_num;
    target_phys_addr_t in_addr[VIRTQUEUE_MAX_SIZE];
    struct iovec in_sg[VIRTQUEUE_MAX_SIZE];
    struct iovec out_sg[VIRTQUEUE_MAX_SIZE];
} VirtQueueElement;

struct VirtIODevice
{
    const char *name;
    uint8_t status;
    uint8_t isr;
    uint16_t queue_sel;
    uint32_t features;
    size_t config_len;
    void *config;
    uint16_t config_vector;
    int nvectors;
    uint32_t (*get_features)(VirtIODevice *vdev);
    uint32_t (*bad_features)(VirtIODevice *vdev);
    void (*set_features)(VirtIODevice *vdev, uint32_t val);
    void (*get_config)(VirtIODevice *vdev, uint8_t *config);
    void (*set_config)(VirtIODevice *vdev, const uint8_t *config);
    void (*reset)(VirtIODevice *vdev);
    VirtQueue *vq;
    const VirtIOBindings *binding;
    void *binding_opaque;
    uint16_t device_id;
};

typedef struct VirtIOBlock
{
    VirtIODevice vdev;
    BlockDriverState *bs;
    VirtQueue *vq;
    void *rq;
    char serial_str[BLOCK_SERIAL_STRLEN + 1];
    QEMUBH *bh;
    size_t config_size;
} VirtIOBlock;

typedef struct VirtIOBlockReq
{
    VirtIOBlock *dev;
    VirtQueueElement elem;
    struct virtio_blk_inhdr *in;
    struct virtio_blk_outhdr *out;
    struct virtio_scsi_inhdr *scsi;
    QEMUIOVector qiov;
    struct VirtIOBlockReq *next;
} VirtIOBlockReq;

typedef struct MultiReqBuffer {
    BlockRequest        blkreq[32];
    int                 num_writes;
    BlockDriverState    *old_bs;
} MultiReqBuffer;

void *cpu_physical_memory_map(target_phys_addr_t addr,
                              target_phys_addr_t *plen,
                              int is_write);
void cpu_physical_memory_unmap(void *buffer, target_phys_addr_t len,
                               int is_write, target_phys_addr_t access_len);


static inline __u64 ldq_phys(target_phys_addr_t pa) {
    __u64 ret;
    memcpy(&ret,(void *)pa,8);
    return ret;
}

static inline __u32 ldl_phys(target_phys_addr_t pa) {
    __u32 ret;
    memcpy(&ret,(void *)pa,4);
    return ret;
}

static inline __u16 lduw_phys(target_phys_addr_t pa) {
    __u16 ret;
    memcpy(&ret,(void *)pa,2);
    return ret;
}

static inline void stl_phys(target_phys_addr_t pa, uint32_t val) {
    memcpy((void *)pa,&val,4);
}

static inline void stw_phys(target_phys_addr_t pa, uint16_t val) {
    memcpy((void *)pa,&val,2);
}

void virtqueue_init(VirtQueue *vq);
void virtqueue_push(VirtQueue *vq, const VirtQueueElement *elem,unsigned int len);
int virtqueue_pop(VirtQueue *vq, VirtQueueElement *elem);
void virtio_notify(VirtQueue *vq);

#endif
