#ifndef QUEMU_VIO_BLK_H
#define QUEMU_VIO_BLK_H

#include <stdint.h>

#include "targphys.h"
#include "block.h"
#include "qemu-vio-virtio.h"

/* Flush the volatile write cache */
#define VIRTIO_BLK_T_FLUSH      4

/* This bit says it's a scsi command, not an actual read or write. */
#define VIRTIO_BLK_T_SCSI_CMD   2

#define QEMU_VIO_BLK_ID_LEN     256     /* length of identify u16 array */

/* These two define direction. */
#define VIRTIO_BLK_T_IN         0
#define VIRTIO_BLK_T_OUT        1

#define VIRTIO_BLK_S_OK         0
#define VIRTIO_BLK_S_IOERR      1
#define VIRTIO_BLK_S_UNSUPP     2
#define WAITING_ASYNC_RET       0xFF

/* Feature bits */
#define VIRTIO_BLK_F_BARRIER    0       /* Does host support barriers? */
#define VIRTIO_BLK_F_SIZE_MAX   1       /* Indicates maximum segment size */
#define VIRTIO_BLK_F_SEG_MAX    2       /* Indicates maximum # of segments */
#define VIRTIO_BLK_F_GEOMETRY   4       /* Indicates support of legacy geometry */
#define VIRTIO_BLK_F_RO         5       /* Disk is read-only */
#define VIRTIO_BLK_F_BLK_SIZE   6       /* Block size of disk is available*/
#define VIRTIO_BLK_F_SCSI       7       /* Supports scsi command passthru */
#define VIRTIO_BLK_F_IDENTIFY   8       /* ATA IDENTIFY supported */
#define VIRTIO_BLK_F_WCACHE     9       /* write cache enabled */

#define VIRTIO_BLK_ID_LEN       256     /* length of identify u16 array */
#define VIRTIO_BLK_ID_SN        10      /* start of char * serial# */
#define VIRTIO_BLK_ID_SN_BYTES  20      /* length in bytes of serial# */


struct qemu_vio_blk_config
{
    uint64_t capacity;
    uint32_t size_max;
    uint32_t seg_max;
    uint16_t cylinders;
    uint8_t heads;
    uint8_t sectors;
    uint32_t _blk_size;    /* structure pad, currently unused */
    uint16_t identify[QEMU_VIO_BLK_ID_LEN];
} __attribute__((packed));

VirtIOBlockReq *virtio_blk_get_request(VirtIOBlock *s);
void virtio_blk_handle_request(VirtIOBlockReq *req, MultiReqBuffer *mrb);
void do_multiwrite(BlockDriverState *bs, BlockRequest *blkreq,int num_writes);
void do_aio_multiwrite(BlockDriverState *bs, MultiReqBuffer *mrb);
void qemu_vio_exit(BlockDriverState *bs);
void qemu_vio_guess_geometry(BlockDriverState *bs, int *pcyls, int *pheads, int *psecs);
int64_t qemu_vio_getlength(BlockDriverState *bs);
BlockErrorAction drive_get_on_error(BlockDriverState *bdrv, int is_read);

#endif /* QUEMU_VIO_H */
