#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <libgen.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/poll.h>
#include <sys/time.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <sys/time.h>
#include <getopt.h>

#include <linux/virtio_blk.h>
#include <linux/virtio_config.h>
#include <linux/virtio_ids.h>
#include <linux/virtio_net.h>
#include <linux/virtio_pci.h>
#include <linux/virtio_vio.h>
#include <linux/virtio_ring.h>

#include "cmd.h"
#include "qemu-common.h"
#include "qemu-vio-blk.h"
#include "qemu-vio.h"
#include "targphys.h"
#include "block_int.h"
#include "qemu-vio-blk.h"

#define VERSION	"2.0"

void *devdata;
BlockDriverState *bs;
VirtIOBlock *VIOblk;
int vdevfd,logfd;

char *progname;
int verbose;
unsigned int bdrv_flags = 0x00;

long long unsigned int num_reads;
long long unsigned int num_writes;
long long unsigned int num_scsi_cmds;
long long unsigned int num_flushs;

unsigned long int bytes_wrote;
unsigned long int bytes_read;

void vv_exit(int param);

target_phys_addr_t data_buffaddr;

#ifdef DEBUG_QEMUVIO
static void printbits(void *buff, int len){
    __u8 *buf = (__u8 *)buff;
    __u8 *cur, tmp;
    int i;

    for (cur=buf; (cur - buf) < len; cur++){
	    tmp = *cur;
	    for(i=7 ; i>=0;i--){
		    printf("%d",((1 << i) & tmp) >> i);
	    }
    }
}

void printConfigs(void *cfgs) {

    struct vv_device_header *hdr;
    struct virtio_blk_config *blkcfg;
    int cfg_data_len;

    cfg_data_len = sizeof(struct vv_device_header) +
                   sizeof(struct virtio_blk_config);
    hdr = (struct vv_device_header *)devdata;
    blkcfg = (struct virtio_blk_config *)(((char *)devdata) + sizeof(struct vv_device_header));

    printf("##############################  config header ##############################\n");
    printf("device_type:%d num_vqs:%d config_len:%d device_status:%d\n",
            hdr->type,hdr->num_vqs,hdr->config_len,hdr->device_status);
    printf("guest features:");printbits(&hdr->guest_features,4);
    printf("\ndevice features:");printbits(&hdr->device_features,4);
    printf("\n############################################################################\n");

}
#endif

/**
 * Qemu-vio side response functions
 * */

static void vv_get_config_hdr(struct swap_buff *vreq) {

    vreq->out_param = 0;
    vreq->len = sizeof(struct vv_device_header);
    vreq->qemu_rsp = VV_GET_CONFIG_HDR_RESPONSE;
    memcpy(vreq->data, devdata, sizeof(struct vv_device_header));
}

static void vv_get_config_space(struct swap_buff *vreq) {

    struct virtio_blk_config *blkcfg;
    blkcfg = (struct virtio_blk_config *)(((char *)devdata) +
            sizeof(struct vv_device_header));

    vreq->out_param = 0;
    vreq->len = sizeof(struct virtio_blk_config);
    vreq->qemu_rsp = VV_GET_CONFIGS_RESPONSE;

    memcpy(vreq->data,blkcfg,vreq->len);
}

static void vv_refresh_config_hdr(struct swap_buff *vreq) {

    memcpy(devdata, vreq->data, sizeof(struct vv_device_header));
    vreq->out_param = 0;
    vreq->len = 0;
    vreq->qemu_rsp = VV_REFRESH_CONFIG_HDR_RESPONSE;
}

static void vv_refresh_config_space(struct swap_buff *vreq) {

    struct virtio_blk_config *blkcfg;
    blkcfg = (struct virtio_blk_config *)(((char *)devdata) +
            sizeof(struct vv_device_header));

    memcpy(blkcfg,vreq->data,sizeof(struct virtio_blk_config));
	  vreq->out_param = 0;
    vreq->len = 0;
	  vreq->qemu_rsp = VV_REFRESH_CONFIG_SPACE_RESPONSE;
}

static void init_vring_space(struct swap_buff *sbuf) {

    VIOblk = malloc(sizeof(struct VirtIOBlock));
    bzero(VIOblk,sizeof(struct VirtIOBlock));
    VIOblk->bs = bs;
    VIOblk->vq = malloc(sizeof(struct VirtQueue));
    bzero(VIOblk->vq,sizeof(struct VirtQueue));

    VIOblk->vdev.name = "qemu-vio-blk-backend";
    VIOblk->vdev.config = (char *)devdata + sizeof(struct vv_device_header);
    VIOblk->vdev.vq = VIOblk->vq;

    VIOblk->vq->vring.num = sbuf->in_param;
    VIOblk->vq->pa = (target_phys_addr_t)((char *)sbuf + RING_OFFSET);
    virtqueue_init(VIOblk->vq);
    VIOblk->vq->inuse = 0;
    VIOblk->vq->last_avail_idx = 0;
    VIOblk->vq->vector = 0;

    num_reads = 0;
    num_writes = 0;
    num_scsi_cmds = 0;
    num_flushs = 0;

    sbuf->qemu_rsp = VV_FIND_VQ_RESPONSE;
}

static void release_vring_space(struct swap_buff *sbuf) {

    if (VIOblk) {
        if (VIOblk->vq)
            free(VIOblk->vq);
        free(VIOblk);
        VIOblk = NULL;
    }

	sbuf->qemu_rsp = VV_DEL_VQ_RESPONSE;
}

static void vv_process_ring(struct swap_buff *vreq) {

    int par = vreq->in_param;
    int i = 0;
    BlockDriverState *old_bs = bs;
    VirtIOBlockReq *req;


    MultiReqBuffer mrb = {
        .num_writes = 0,
        .old_bs = bs,
    };

    pdebug("qemu-vio: ==== %s: processing kick %d ==== \n",
            __func__, (int)vreq->in_param);

    while ((req = virtio_blk_get_request(VIOblk))) {
        i++;
        virtio_blk_handle_request(req, &mrb);
    }

    async_context_push();
    do_aio_multiwrite(old_bs, &mrb);
    async_context_pop();

    vreq->out_param = par;
    vreq->len = i;
    vreq->qemu_rsp = VV_KICK_DONE;
}

static void vv_server_reset(struct swap_buff *vreq) {

    fprintf(stdout,"qemu-vio: reseting server ");
    fprintf(stdout,"num_reads: %llu, num_writes: %llu, num_scsi_cmds: %llu,num_flushs: %llu\n",
           num_reads, num_writes,num_scsi_cmds, num_flushs );

    release_vring_space(vreq);
    init_vring_space(vreq);

    vreq->qemu_rsp = VV_SERVER_RESET_DONE;
}

/**
 * Qemu-vio functions
 * */

static void usage(char *pname){

    printf("Usage: %s: [-h] [-V] [-v] [-f <cache_flag>] <virtio device> <imagefile>\n",pname);
    printf("Qemu-vio virtual disk backend\n");
    printf("	-h, --help      display this help and exit\n");
    printf("	-V, --version   output version information and exit\n");
    printf("	-v, --verbose   run qemu-vio in verbose mode\n");
    printf("	-f, --flags     run qemu-vio with one of following cache flags: writeback, native_aio or nocache\n");
}

void vv_exit(int param) {

    free(devdata);
    fprintf(stdout,"\nqemu-vio: ");
    fprintf(stdout,"num_reads: %llu, num_writes: %llu, num_scsi_cmds: %llu,num_flushs: %llu\n",
           num_reads, num_writes,num_scsi_cmds, num_flushs );
    fprintf(stdout,"qemu-vio: total requests processed: %llu\n",
            num_reads+num_writes+num_scsi_cmds+num_flushs);
    fprintf(stdout,"qemu-vio: bye.\n");
    exit(0);
}

static unsigned int get_features(void) {

    unsigned int flags;

    flags = 0;

    flags |= (1 << VIRTIO_BLK_F_SEG_MAX);
    flags |= VIRTIO_BLK_F_BLK_SIZE;
    flags |= VIRTIO_BLK_F_SIZE_MAX;
    flags |= VIRTIO_RING_F_INDIRECT_DESC;


    if (bdrv_enable_write_cache(bs))
        flags |= (1 << VIRTIO_BLK_F_WCACHE);

#ifdef __linux__
    flags |= (1 << VIRTIO_BLK_F_SCSI);
#endif

    if (bdrv_is_read_only(bs))
        flags |= 1 << VIRTIO_BLK_F_RO;

    return flags;
}

static int gather_device_configs(void) {

    struct vv_device_header *hdr;
    struct virtio_blk_config *blkcfg;
    int cfg_data_len;
    int tcyls, theads, tsectors;

    cfg_data_len = sizeof(struct vv_device_header) +
                   sizeof(struct virtio_blk_config);
    memset(devdata, 0x00, cfg_data_len);
    hdr = (struct vv_device_header *)devdata;
    blkcfg = (struct virtio_blk_config *)(((char *)devdata) +
            sizeof(struct vv_device_header));

    hdr->type = VIRTIO_ID_BLOCK;
    hdr->num_vqs = 1;
    hdr->vqs_size = VRING_ELEMENTS;
    hdr->device_features = get_features();
    hdr->guest_features = 0;
    hdr->config_len = sizeof(struct virtio_blk_config);
    hdr->device_status = 0;

    blkcfg->capacity = qemu_vio_getlength(bs);
    blkcfg->size_max = 4096;
    blkcfg->blk_size = 512;
    blkcfg->seg_max = VRING_ELEMENTS - 2;
    qemu_vio_guess_geometry(bs, &tcyls, &theads, &tsectors);
    blkcfg->geometry.cylinders = tcyls;
    blkcfg->geometry.heads = theads;
    blkcfg->geometry.sectors = tsectors;

    fprintf(stdout,"qemu-vio: total sectors: %llu\n",
        (long long)blkcfg->capacity);
    return 0;
}

static void process_swap_buff(struct swap_buff *vreq){
	switch(vreq->qemu_cmd) {
    case VV_GET_CONFIG_HDR:
        vv_get_config_hdr(vreq);
        break;
    case VV_GET_CONFIGS:
        vv_get_config_space(vreq);
        break;
    case VV_REFRESH_CONFIG_HDR:
        vv_refresh_config_hdr(vreq);
        break;
    case VV_REFRESH_CONFIG_SPACE:
        vv_refresh_config_space(vreq);
        break;
    case VV_FIND_VQ:
        init_vring_space(vreq);
        break;
    case VV_DEL_VQ:
        release_vring_space(vreq);
        break;
    case VV_RING_KICK:
	  case VV_NOTIFY:
		vv_process_ring(vreq);
        break;
    case VV_SERVER_RESET:
        vv_server_reset(vreq);
        break;
    default:
	fprintf(stderr,"qemu-vio: Got invalid option %d\n",vreq->qemu_cmd);
	exit(1);
	break;
  }
}

static int open_image(char *name) {

    bdrv_flags |= BDRV_O_RDWR;

    if (bs) {
        fprintf(stderr,"qemu-vio: file open already, try 'help close'\n");
        return 1;
    }

    bs = bdrv_new("hda");
    if (!bs)
        return 1;

    if (bdrv_open(bs, name, bdrv_flags, NULL) == -1) {
        fprintf(stderr,"qemu-vio: can't open file %s\n", name);
        bs = NULL;
        return 1;
    }

    return 0;
}

/**
 * Main loop
 * */
int main(int argc, char **argv) {

    int  c,nbytes,opt_index;
    char *sh_buff, imgname[30], devname[30];
    struct pollfd pfd1;
    struct swap_buff *vreq;
    struct timeval t1, t2;
    long long unsigned int last_num_reads;
    long long unsigned int last_num_writes;
    const char *svioopt = "hVvf:";
    const struct option vioopt[] = {
	    { "help", 0, NULL, 'h' },
	    { "version", 0, NULL, 'V' },
	    { "flags", 1, NULL, 'f' },
	    { "verbose", 0, NULL, 'v' },
	    { NULL, 0, NULL, 0 }
    };

    progname = basename(argv[0]);
    verbose = 0;

    while ((c = getopt_long(argc, argv, svioopt, vioopt, &opt_index)) != -1) {
        switch (c) {
            case 'h':
                usage(progname);
                exit(0);
                break;
            case 'V':
                printf("%s version %s\n", progname, VERSION);
                exit(0);
            case 'v':
                verbose = 1;
                break;
            case 'f':
                if(!strcmp(optarg,"writeback")) {
                   bdrv_flags |= BDRV_O_CACHE_WB;
                } else if (!strcmp(optarg,"native_aio")) {
                   bdrv_flags |= BDRV_O_NATIVE_AIO;
                } else if (!strcmp(optarg,"nocache")) {
                   bdrv_flags |= BDRV_O_NOCACHE;
                } else {
                   printf("Invalid flag parameter.\n");
                   usage(progname);
                   exit(1);
                }
                break;
            default:
                usage(progname);
                exit(1);
        }
    }

    if ((argc - optind) != 2) {
	      usage(progname);
	      exit(1);
    }

    strcpy(devname,argv[optind++]);
    strcpy(imgname,argv[optind]);

    if (signal(SIGINT, vv_exit)==SIG_ERR) {
        fprintf(stderr,"qemu-vio: error capturing signal");
        return(1);
    }

    bdrv_init();
    if (open_image(imgname)) {
        fprintf(stderr,"qemu-vio: could not open file %s\n", imgname);
        return 0;
    }

    devdata = malloc(sizeof(struct vv_device_header)+
            sizeof(struct virtio_blk_config));

    gather_device_configs();

#ifdef DEBUG_QEMUVIO
    printConfigs(devdata);
#endif

    vdevfd = open(devname, O_RDWR | O_SYNC);
    if ( vdevfd == -1) {
        fprintf(stderr,"qemu-vio: open. Check if the module virtio_vios is loaded");
        exit(0);
    }

    /*
     * This memory mapped pointer 'sh_buff' points to the same chunk of memory
     * allocated by virtio_vios. The layout of the shared buffer is described 
     * in /usr/include/linux/virtio_vio.h
     **/
    sh_buff = mmap(0,SWAP_SIZE,
                PROT_READ | PROT_WRITE,
                MAP_FILE | MAP_SHARED,
                vdevfd, 0);
    if (sh_buff == MAP_FAILED) {
        fprintf(stderr,"qemu-vio: mmap() failed\n");
        exit(1);
    }
    pfd1.fd = vdevfd;
    pfd1.events = POLLIN;

    data_buffaddr = (target_phys_addr_t)((char *)sh_buff +
                RING_DATABUF_OFFSET);
    vreq = (struct swap_buff *)sh_buff;

    num_reads = 0;
    num_writes = 0;
    num_scsi_cmds = 0;
    num_flushs = 0;
    bytes_wrote = 0;
    bytes_read = 0;
    gettimeofday(&t1, 0);
    gettimeofday(&t2, 0);
    last_num_reads = num_reads;
    last_num_writes = num_writes;

    fprintf(stdout,"qemu-vio: checking/waiting for data...");
    fflush(stdout);
    while (1) {
        //This read wont block if we got more request in vring
        nbytes = read(vdevfd,NULL,0);

	      if((toSeconds(t2) - toSeconds(t1)) >= 1) {
		       fprintf(stdout,"Read rate: %f MB/s			",
			       ((float)(num_reads - last_num_reads)/10)/(toSeconds(t2)-toSeconds(t1)));
		       fprintf(stdout,"Write rate: %f MB/s\n",
			       ((float)(num_writes - last_num_writes)/10)/(toSeconds(t2)-toSeconds(t1)));
		       gettimeofday(&t1, 0);
		       last_num_reads = num_reads;
		       last_num_writes = num_writes;
	      }

        process_swap_buff(vreq);
        gettimeofday(&t2, 0);

	      pdebug("Elapsed time: %f sec\n", toSeconds(t2)-toSeconds(t1));
        pdebug("%lu bytes read, %lu bytes wrote\n",bytes_read,bytes_wrote);
	      fflush(stdout);
        //Finish the request processing in kernel space
        ioctl(vdevfd, DATAREADY,0);
    }
}
