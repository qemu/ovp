#include "qemu-vio-virtio.h"
#include <linux/types.h>
#include <string.h>
#include <stdio.h>
#include <linux/virtio_vio.h>
#include <linux/virtio_ring.h>


extern target_phys_addr_t data_buffaddr;

//#define trace() printf("QEMU-VIO-VIRTIO: %s-%s: %s(%d)\n",__TIME__, __FILE__,__func__, __LINE__)
#define trace() ;

#define printk printf

static inline target_phys_addr_t vring_align(target_phys_addr_t addr,
        unsigned long align)
{
    return (addr + align - 1) & ~(align - 1);
}

static inline uint64_t vring_desc_addr(target_phys_addr_t desc_pa, int i)
{
    target_phys_addr_t pa,tmp;
    pa = desc_pa + sizeof(VRingDesc) * i + offsetof(VRingDesc, addr);

    tmp = ldq_phys(pa);

    return ldq_phys(pa);
}

static inline uint32_t vring_desc_offset(target_phys_addr_t desc_pa, int i)
{
    target_phys_addr_t pa;
    pa = desc_pa + sizeof(VRingDesc) * i + offsetof(VRingDesc, offset);
    return ldl_phys(pa);
}

static inline uint32_t vring_desc_poolidx(target_phys_addr_t desc_pa, int i)
{
    target_phys_addr_t pa;
    pa = desc_pa + sizeof(VRingDesc) * i + offsetof(VRingDesc, poolidx);
    return ldl_phys(pa);
}

static inline uint32_t vring_desc_len(target_phys_addr_t desc_pa, int i)
{
    target_phys_addr_t pa;
    pa = desc_pa + sizeof(VRingDesc) * i + offsetof(VRingDesc, len);

    return ldl_phys(pa);
}

static inline uint16_t vring_desc_flags(target_phys_addr_t desc_pa, int i)
{
    target_phys_addr_t pa;
    pa = desc_pa + sizeof(VRingDesc) * i + offsetof(VRingDesc, flags);

    return lduw_phys(pa);
}

static inline uint16_t vring_desc_next(target_phys_addr_t desc_pa, int i)
{
    target_phys_addr_t pa;



    pa = desc_pa + sizeof(VRingDesc) * i + offsetof(VRingDesc, next);

    return lduw_phys(pa);
}

static inline uint16_t vring_avail_flags(VirtQueue *vq)
{
    target_phys_addr_t pa;
    pa = vq->vring.avail + offsetof(VRingAvail, flags);

    return lduw_phys(pa);
}

static inline uint16_t vring_avail_idx(VirtQueue *vq)
{
    target_phys_addr_t pa;

    pa = vq->vring.avail + offsetof(VRingAvail, idx);

    return lduw_phys(pa);
}

static inline uint16_t vring_avail_ring(VirtQueue *vq, int i)
{
    target_phys_addr_t pa;
    pa = vq->vring.avail + offsetof(VRingAvail, ring[i]);

    return lduw_phys(pa);
}

static inline void vring_used_ring_id(VirtQueue *vq, int i, uint32_t val)
{
    target_phys_addr_t pa;
    pa = vq->vring.used + offsetof(VRingUsed, ring[i].id);
    stl_phys(pa, val);
}

static inline void vring_used_ring_len(VirtQueue *vq, int i, uint32_t val)
{
    target_phys_addr_t pa;
    pa = vq->vring.used + offsetof(VRingUsed, ring[i].len);
    stl_phys(pa, val);
}

static uint16_t vring_used_idx(VirtQueue *vq)
{
    target_phys_addr_t pa;
    pa = vq->vring.used + offsetof(VRingUsed, idx);

    return lduw_phys(pa);
}

static inline void vring_used_idx_increment(VirtQueue *vq, uint16_t val)
{
    target_phys_addr_t pa;
    pa = vq->vring.used + offsetof(VRingUsed, idx);
    stw_phys(pa, vring_used_idx(vq) + val);
}

static inline void vring_used_flags_set_bit(VirtQueue *vq, int mask)
{
    target_phys_addr_t pa;
    pa = vq->vring.used + offsetof(VRingUsed, flags);
    stw_phys(pa, lduw_phys(pa) | mask);
}

static inline void vring_used_flags_unset_bit(VirtQueue *vq, int mask)
{
    target_phys_addr_t pa;
    pa = vq->vring.used + offsetof(VRingUsed, flags);
    stw_phys(pa, lduw_phys(pa) & ~mask);
}

static inline void virtio_queue_set_notification(VirtQueue *vq, int enable)
{
    if (enable)
        vring_used_flags_unset_bit(vq, VRING_USED_F_NO_NOTIFY);
    else
        vring_used_flags_set_bit(vq, VRING_USED_F_NO_NOTIFY);
}

static unsigned int virtqueue_get_head(VirtQueue *vq, unsigned int idx)
{
    unsigned int head;

    // Grab the next descriptor number they're advertising, and increment
    // the index we've seen.
    head = vring_avail_ring(vq, idx % vq->vring.num);

    // If their number is silly, that's a fatal mistake.
    if (head >= vq->vring.num) {
        fprintf(stderr, "Guest says index %u is available", head);
        return -1;
    }

    return head;
}

static int virtqueue_num_heads(VirtQueue *vq, unsigned int idx)
{
    uint16_t num_heads = vring_avail_idx(vq) - idx;

    //Check it isn't doing very strange things with descriptor numbers.
    if (num_heads > vq->vring.num) {
        fprintf(stderr, "Guest moved used index from %u to %u",
                idx, vring_avail_idx(vq));
        return -1;
    }

    return num_heads;
}

static unsigned virtqueue_next_desc(target_phys_addr_t desc_pa,
                                    unsigned int i, unsigned int max)
{
    unsigned int next;

    //If this descriptor says it doesn't chain, we're done.
    if (!(vring_desc_flags(desc_pa, i) & VRING_DESC_F_NEXT))
        return max;

    // Check they're not leading us off end of descriptors.
    next = vring_desc_next(desc_pa, i);
    // Make sure compiler knows to grab that: we don't want it changing!
    //wmb();

    if (next >= max) {
        fprintf(stderr, "Desc next is %u", next);
        return -1;
    }

    return next;
}

static void virtqueue_fill(VirtQueue *vq, const VirtQueueElement *elem,
                           unsigned int len, unsigned int idx)
{
    idx = (idx + vring_used_idx(vq)) % vq->vring.num;

    /* Get a pointer to the next entry in the used ring. */
    vring_used_ring_id(vq, idx, elem->index);
    vring_used_ring_len(vq, idx, len);
}

static void virtqueue_flush(VirtQueue *vq, unsigned int count)
{
    /* Make sure buffer is written before we update index. */
    //wmb();

    vring_used_idx_increment(vq, count);
    vq->inuse -= count;
}

void virtqueue_init(VirtQueue *vq)
{
    target_phys_addr_t pa = vq->pa;

    vq->vring.desc = pa;
    vq->vring.avail = pa + vq->vring.num * sizeof(VRingDesc);
    vq->vring.used = vring_align(vq->vring.avail +
                                 offsetof(VRingAvail, ring[vq->vring.num]),
                                 VIRTIO_PCI_VRING_ALIGN);
}

int virtqueue_pop(VirtQueue *vq, VirtQueueElement *elem)
{
    unsigned int i, head, max;
    target_phys_addr_t desc_pa = vq->vring.desc;
    target_phys_addr_t len;




    if (!virtqueue_num_heads(vq, vq->last_avail_idx)){
        return 0;
    }

    // When we start there are none of either input nor output.
    elem->out_num = elem->in_num = 0;

    max = vq->vring.num;


    i = head = virtqueue_get_head(vq, vq->last_avail_idx++);


    if (vring_desc_flags(desc_pa, i) & VRING_DESC_F_INDIRECT) {
        if (vring_desc_len(desc_pa, i) % sizeof(VRingDesc)) {
            fprintf(stderr, "Invalid size for indirect buffer table\n");
            return -1;
        }

        // loop over the indirect descriptor table
        max = vring_desc_len(desc_pa, i) / sizeof(VRingDesc);
        desc_pa = vring_desc_addr(desc_pa, i);
        i = 0;
    }


    do {
        struct myiovec *sg;
        int is_write = 0;

	if (vring_desc_flags(desc_pa, i) & VRING_DESC_F_WRITE) {
            elem->in_addr[elem->in_num] = vring_desc_addr(desc_pa, i);
            sg = (struct myiovec *)&elem->in_sg[elem->in_num++];
            is_write = 1;
        } else
            sg = (struct myiovec *)&elem->out_sg[elem->out_num++];

        // Grab the first descriptor, and check it's OK.
        sg->iov_len = vring_desc_len(desc_pa, i);
        len = sg->iov_len;
        //Check with our function

	sg->iov_base = (void *)(vring_desc_offset(desc_pa, i) + (char *)data_buffaddr);

        if (sg->iov_base == NULL || len != sg->iov_len) {
            fprintf(stderr, "qemu-vio-virtio: trying to map MMIO memory\n");
            return -1;
        }

        // If we've got too many, that implies a descriptor loop.
        if ((elem->in_num + elem->out_num) > max) {
            fprintf(stderr, "Looped descriptor");
            return -1;
        }
    } while ((i = virtqueue_next_desc(desc_pa, i, max)) != max);

    elem->index = head;

    vq->inuse++;

    return elem->in_num + elem->out_num;
}

void virtqueue_push(VirtQueue *vq, const VirtQueueElement *elem,
                    unsigned int len)
{
    virtqueue_fill(vq, elem, len, 0);

    virtqueue_flush(vq, 1);

}

void virtio_notify(VirtQueue *vq)
{


}
